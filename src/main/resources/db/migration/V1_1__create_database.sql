DROP TABLE if EXISTS person;
DROP TABLE if EXISTS person_work_group;
DROP TABLE if EXISTS work_group;

CREATE TABLE person
(
	id BIGINT NOT NULL AUTO_INCREMENT,
	firstname VARCHAR(250),
	lastname VARCHAR(250),
 	email VARCHAR(250),
 	PRIMARY KEY (id),
	UNIQUE (email)
);

CREATE TABLE team
(
	id BIGINT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	description VARCHAR(250),
	PRIMARY KEY (id),
	UNIQUE (name)
);

CREATE TABLE person_team
(
	person_fk BIGINT NOT NULL,
	team_fk BIGINT NOT NULL,
	FOREIGN KEY(person_fk) REFERENCES person(id) ON DELETE cascade,
	FOREIGN KEY(team_fk) REFERENCES team(id) ON DELETE cascade
);
