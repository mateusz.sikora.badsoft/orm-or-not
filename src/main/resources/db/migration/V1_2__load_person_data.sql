INSERT INTO team (name, description) VALUES
('SYS_ADMIN', 'System admins'),
('PROJECT_ADMIN', 'Project admin'),
('DEVELOPERS', 'Developers'),
('VIEWERS', 'Viewers');

INSERT INTO person (firstname, lastname, email) VALUES
 ('Mateusz', 'Sikora', 'msikora@testmail.com'),
 ('Adam', 'Sikora', 'asikora@testmail.com'),
 ('Janusz', 'Nowak', 'jnowak@testmail.com'),
 ('Marta', 'Kowalska', 'mkowalska@testmail.com');


INSERT INTO person_team (person_fk, team_fk)
SELECT (SELECT id FROM person WHERE email = 'msikora@testmail.com'), (SELECT id FROM team WHERE name = 'SYS_ADMIN');

INSERT INTO person_team (person_fk, team_fk)
SELECT (SELECT id FROM person WHERE email = 'asikora@testmail.com'), (SELECT id FROM team WHERE name = 'PROJECT_ADMIN');

INSERT INTO person_team (person_fk, team_fk)
SELECT (SELECT id FROM person WHERE email = 'asikora@testmail.com'), (SELECT id FROM team WHERE name = 'DEVELOPERS');

