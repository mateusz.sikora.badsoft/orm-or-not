package pl.badsoft.edu.person;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.Getter;
import lombok.Setter;
import pl.badsoft.edu.person.dto.PersonDto;
import pl.badsoft.edu.person.dto.TeamDto;

@Getter
@Setter
@Entity(name = "person")
@Table(name = "person")
@NamedEntityGraph(name = "Person.teams", attributeNodes = @NamedAttributeNode("teams"))
class Person {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false, updatable = false)
	private Long id;

	@Column(name = "firstname", length = 250)
	private String firstname;

	@Column(name = "lastname", length = 250)
	private String lastname;

	@Column(name = "email", length = 250, unique = true)
	private String email;

	@ManyToMany
	@JoinTable(name = "person_team",
			joinColumns = @JoinColumn(name = "person_fk", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "team_fk", referencedColumnName = "id"))
	private Set<Team> teams = new HashSet<>();

	PersonDto mapToDto() {
		PersonDto personDto = new PersonDto(firstname, lastname, email);
		Set<TeamDto> teamDtos = teams.stream()
				.map(team -> team.mapToDto())
				.collect(Collectors.toSet());
		personDto.setTeams(teamDtos);
		return personDto;
	}
}
