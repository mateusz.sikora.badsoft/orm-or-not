package pl.badsoft.edu.person;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import pl.badsoft.edu.person.dto.PersonDto;
import pl.badsoft.edu.person.dto.TeamDto;

@RequiredArgsConstructor
@Slf4j
@Component
class PersonDao {

	private final NamedParameterJdbcTemplate paramJdbcTemplate;

	private static final String SELECT_PERSON_BY_ID = """
			SELECT * FROM person WHERE id = :id;
			""";

	private static final String SELECT_PERSONS = """
			SELECT * FROM person;
			""";

	private static final String SELECT_PERSON_GROUP_BY_ID = """
			SELECT * FROM person p
			LEFT JOIN person_team pt ON p.id = pt.person_fk
			LEFT JOIN team t ON pt.team_fk = t.id
			WHERE p.id = :id
			""";

	private static final String SELECT_PERSONS_EMAILS = """
			SELECT email FROM person;
			""";

	Optional<PersonDto> findPersonById(Long id) {
		List<PersonDto> result = paramJdbcTemplate.query(SELECT_PERSON_BY_ID,
				new MapSqlParameterSource().addValue("id", id),
				BeanPropertyRowMapper.newInstance(PersonDto.class));
		return Optional.ofNullable(!result.isEmpty() ? result.get(0) : null);
	}

	List<PersonDto> findAllPersons() {
		return paramJdbcTemplate.query(SELECT_PERSONS,
				BeanPropertyRowMapper.newInstance(PersonDto.class));
	}

	Optional<PersonDto> findPersonDtoById(Long id) {
		PersonDto personDto = paramJdbcTemplate.queryForObject(SELECT_PERSON_GROUP_BY_ID,
				new MapSqlParameterSource().addValue("id", id),
				new PersonRowMapper());
		return Optional.ofNullable(personDto);
	}

	List<String> findAllPersonsEmails() {
		return paramJdbcTemplate.query(SELECT_PERSONS_EMAILS,
				(rs, rowNum) -> rs.getString("email"));
	}

	private static class PersonRowMapper implements RowMapper<PersonDto> {

		@Override
		public PersonDto mapRow(ResultSet rs, int rowNum) throws SQLException {
			PersonDto personDto = new PersonDto();
			personDto.setFirstname(rs.getString("firstname"));
			personDto.setLastname(rs.getString("lastname"));
			personDto.setEmail(rs.getString("email"));
			Set<TeamDto> teams = new HashSet<>();
			personDto.setTeams(teams);
			do {
				String name = rs.getString("name");
				String description = rs.getString("description");
				if (StringUtils.isNoneEmpty(name, description)) {
					teams.add(new TeamDto(name, description));
				}
			} while (rs.next());
			return personDto;
		}
	}
}
