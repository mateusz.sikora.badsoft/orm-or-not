package pl.badsoft.edu.person;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.badsoft.edu.person.dto.PersonDto;

@RestController
@RequestMapping("/api/persons/data")
@AllArgsConstructor
@Slf4j
class PersonController {

	private PersonRepository personRepository;

	@GetMapping()
		//N+1 problem
	ResponseEntity<Collection<PersonDto>> getPersons() {
		LocalDateTime start = LocalDateTime.now();
		Collection<Person> persons = personRepository.findAll();
		long executedTime = ChronoUnit.MILLIS.between(start, LocalDateTime.now());

		Collection<PersonDto> result = persons.stream()
				.map(person -> person.mapToDto())
				.collect(Collectors.toSet());

		log.info("[SPRING-DATA] Get all persons in {} ms", executedTime);
		return ResponseEntity.ok(result);
	}

	@GetMapping("/joinfetch")
		//Solution 1: use join fetch
	ResponseEntity<Collection<PersonDto>> getPersonsJoinFetch() {
		LocalDateTime start = LocalDateTime.now();
		Set<PersonDto> result = personRepository.findAllByJoinFetch().stream()
				.map(person -> person.mapToDto()).collect(Collectors.toSet());
		long executedTime = ChronoUnit.MILLIS.between(start, LocalDateTime.now());
		log.info("[SPRING-DATA_JOIN-FETCH] Get all persons in {} ms", executedTime);
		return ResponseEntity.ok(result);
	}

	@GetMapping("/entitygraph")
		//Solution 2: use entiy graph
	ResponseEntity<Collection<PersonDto>> getPersonsEntityGraph() {
		LocalDateTime start = LocalDateTime.now();
		Set<PersonDto> result = personRepository.findAllByEntityGraph().stream()
				.map(person -> person.mapToDto()).collect(Collectors.toSet());
		long executedTime = ChronoUnit.MILLIS.between(start, LocalDateTime.now());
		log.info("[SPRING-DATA_ENTITY-GRAPH] Get all persons in {} ms", executedTime);
		return ResponseEntity.ok(result);
	}

	@GetMapping("/{id}")
	ResponseEntity<PersonDto> getPerson(@PathVariable Long id) {
		LocalDateTime start = LocalDateTime.now();
		Optional<Person> optPerson = personRepository.findById(id);

		if (optPerson.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		PersonDto personDto = optPerson.get().mapToDto();
		long executedTime = ChronoUnit.MILLIS.between(start, LocalDateTime.now());
		log.info("[SPRING-DATA] Get person {} in {} ms", id, executedTime);
		return ResponseEntity.ok(personDto);
	}

	@GetMapping("/{id}/joinfetch")
	ResponseEntity<PersonDto> getPersonJoinFetch(@PathVariable Long id) {
		LocalDateTime start = LocalDateTime.now();
		Optional<Person> optPerson = personRepository.findByJoinFetch(id);

		if (optPerson.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		PersonDto personDto = optPerson.get().mapToDto();
		long executedTime = ChronoUnit.MILLIS.between(start, LocalDateTime.now());
		log.info("[SPRING-DATA_JOIN-FETCH] Get person {} in {} ms", id, executedTime);
		return ResponseEntity.ok(personDto);
	}

	@GetMapping("/{id}/entitygraph")
	ResponseEntity<PersonDto> getPersonEntityGraph(@PathVariable Long id) {
		LocalDateTime start = LocalDateTime.now();
		Optional<Person> optPerson = personRepository.findByEntityGraph(id);

		if (optPerson.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		PersonDto personDto = optPerson.get().mapToDto();
		long executedTime = ChronoUnit.MILLIS.between(start, LocalDateTime.now());
		log.info("[SPRING-DATA_ENTITY-GRAPH] Get person {} in {} ms", id, executedTime);
		return ResponseEntity.ok(personDto);
	}

	@GetMapping("/all")
		//Mapping to DTO in Repository
	ResponseEntity<List<PersonDto>> getPersonsByRepositoryMapping() {
		List<PersonDto> persons = personRepository.findAllPersonDto();
		if (persons.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(persons);
	}
}
