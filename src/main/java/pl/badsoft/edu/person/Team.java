package pl.badsoft.edu.person;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;
import pl.badsoft.edu.person.dto.TeamDto;

@Getter
@Setter
@Entity(name = "team")
@Table(name = "team")
class Team {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false, updatable = false)
	private Long id;

	@Column(name = "name", length = 50, unique = true)
	private String name;

	@Column(name = "description", length = 250)
	private String description;

	@ManyToMany(mappedBy = "teams")
	private Set<Person> members = new HashSet<>();

	TeamDto mapToDto() {
		TeamDto teamDto = new TeamDto();
		teamDto.setName(name);
		teamDto.setDescription(description);
		return teamDto;
	}
}
