package pl.badsoft.edu.person.dto;

import java.util.HashSet;
import java.util.Set;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PersonDto {
	private String firstname;
	private String lastname;
	private String email;
	private Set<TeamDto> teams = new HashSet<>();

	public PersonDto(String firstname, String lastname, String email) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
	}
}
