package pl.badsoft.edu.person;

import org.springframework.data.jpa.repository.JpaRepository;

interface TeamRepository extends JpaRepository<Team, Long> {
}