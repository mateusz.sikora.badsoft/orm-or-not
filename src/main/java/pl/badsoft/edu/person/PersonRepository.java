package pl.badsoft.edu.person;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.badsoft.edu.person.dto.PersonDto;

interface PersonRepository extends JpaRepository<Person, Long> {

	@EntityGraph(value = "Person.teams", type = EntityGraph.EntityGraphType.LOAD)
	@Query("SELECT p FROM person p")
	List<Person> findAllByEntityGraph();

	@EntityGraph(value = "Person.teams", type = EntityGraph.EntityGraphType.LOAD)
	@Query("SELECT p FROM person p WHERE id = :id")
	Optional<Person> findByEntityGraph(Long id);

	@Query("SELECT DISTINCT p FROM person p JOIN FETCH p.teams")
	List<Person> findAllByJoinFetch();

	@Query("SELECT DISTINCT p FROM person p JOIN FETCH p.teams WHERE p.id = :id")
	Optional<Person> findByJoinFetch(Long id);

	@Query("""
			SELECT new pl.badsoft.edu.person.dto.PersonDto(p.firstname, p.lastname, p.email)
			FROM person p
			""")
	List<PersonDto> findAllPersonDto();
}