package pl.badsoft.edu.person;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.badsoft.edu.person.dto.PersonDto;

@RestController
@RequestMapping("/api/persons/jdbc")
@AllArgsConstructor
@Slf4j
class PersonControllerJdbc {

	private PersonDao personDao;

	@GetMapping()
	ResponseEntity<Collection<PersonDto>> getPersonsByDao() {
		LocalDateTime start = LocalDateTime.now();
		List<PersonDto> persons = personDao.findAllPersons();
		long executedTime = ChronoUnit.MILLIS.between(start, LocalDateTime.now());
		log.info("[JDBC] Get all persons {} ms", executedTime);

		if (persons.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(persons);
	}

	@GetMapping("/{id}")
	ResponseEntity<PersonDto> getPersonByDao(@PathVariable Long id) {
		LocalDateTime start = LocalDateTime.now();
		Optional<PersonDto> optPerson = personDao.findPersonById(id);

		long executedTime = ChronoUnit.MILLIS.between(start, LocalDateTime.now());
		log.info("[JDBC] Get person {} in {} ms", id, executedTime);

		return optPerson.map(personDto -> ResponseEntity.ok(personDto)).orElseGet(() -> ResponseEntity.notFound().build());
	}

	@GetMapping("/dto/{id}")
	ResponseEntity<PersonDto> getPersonDtoByDao(@PathVariable Long id) {
		LocalDateTime start = LocalDateTime.now();
		Optional<PersonDto> optPerson = personDao.findPersonDtoById(id);

		long executedTime = ChronoUnit.MILLIS.between(start, LocalDateTime.now());
		log.info("[JDBC] Get person {} in {} ms", id, executedTime);

		return optPerson.map(personDto -> ResponseEntity.ok(personDto)).orElseGet(() -> ResponseEntity.notFound().build());
	}

	@GetMapping("/emails")
	ResponseEntity<List<String>> getPersonsEmails() {
		LocalDateTime start = LocalDateTime.now();
		List<String> emails = personDao.findAllPersonsEmails();

		long executedTime = ChronoUnit.MILLIS.between(start, LocalDateTime.now());
		log.info("[JDBC] Get all persons emails in {} ms", executedTime);
		return ResponseEntity.ok(emails);
	}

}
